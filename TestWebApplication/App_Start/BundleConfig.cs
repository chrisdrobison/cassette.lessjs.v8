﻿using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace TestWebApplication
{
    public class BundleConfig : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            RegisterBundles(bundles);
        }

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add<ScriptBundle>("~/bundles/jquery", new[]
            {
                "~/Scripts/jquery-1.10.2.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js"
            }, bundle => bundle.PageLocation = "body");

            bundles.Add<ScriptBundle>("~/bundles/modernizr", new[]
            {
                "~/Scripts/modernizr-2.6.2.js"
            });

            bundles.Add<ScriptBundle>("~/bundles/bootstrap", new[]
            {
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"
            }, bundle => bundle.PageLocation = "body");

            bundles.Add<StylesheetBundle>("~/Content/css", new[]
            {
                "~/Content/bootstrap/bootstrap.less",
                "~/Content/site.css"
            });
        }
    }
}