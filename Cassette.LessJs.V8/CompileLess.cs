﻿using System;
using Cassette.BundleProcessing;
using Cassette.Stylesheets;

namespace Cassette.LessJs.V8
{
    public class CompileLess : IBundleProcessor<StylesheetBundle>
    {
        private readonly ILessCompiler _lessCompiler;
        private readonly CassetteSettings _settings;

        public CompileLess(ILessCompiler lessCompiler, CassetteSettings settings)
        {
            _lessCompiler = lessCompiler;
            _settings = settings;
        }

        public void Process(StylesheetBundle bundle)
        {
            foreach (var asset in bundle.Assets)
            {
                if (asset.Path.EndsWith(".less", StringComparison.OrdinalIgnoreCase))
                {
                    asset.AddAssetTransformer(new CompileAsset(_lessCompiler, _settings.SourceDirectory));
                }
            }
        }
    }
}