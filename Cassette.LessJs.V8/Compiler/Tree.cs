﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace Cassette.LessJs.V8.Compiler
{
    public class Tree
    {
        private readonly dynamic _jsTree;

        public Tree(dynamic jsTree, dynamic parserJs)
        {
            _jsTree = jsTree;

            DynamicObject files = parserJs.imports.files;

            Dependencies = from f in files.GetDynamicMemberNames().ToList()
                select new Uri(f).LocalPath;
        }

        public IEnumerable<string> Dependencies { get; private set; }

        public string ToCss()
        {
            return (string) _jsTree.toCSS();
        }
    }
}