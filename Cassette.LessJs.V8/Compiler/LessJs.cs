﻿using System;
using System.IO;
using System.Reflection;
using System.Web;
using Cassette.LessJs.V8.Compiler.Stubs;
using Microsoft.ClearScript;
using Microsoft.ClearScript.V8;

namespace Cassette.LessJs.V8.Compiler
{
    public class LessJs
    {
        private const String ClearScriptLibPrefix = "ClearScriptV8";
        private const String ResourcePrefix = "Cassette.LessJs.V8.Resources";
        private const String LessJsResource = ResourcePrefix + ".less-1.6.3.min.js";
        private static readonly ScriptEngine LessJsSingleton;

        private static readonly string[] LibResources =
        {
            ClearScriptLibPrefix + "-32.dll",
            ClearScriptLibPrefix + "-64.dll",
            "v8-ia32.dll",
            "v8-x64.dll",
        };

        static LessJs()
        {
            // Extract ClearScript assemblies out if they aren't already there
            foreach (var libResource in LibResources)
            {
                var file = new FileInfo(Path.Combine(HttpRuntime.CodegenDir, libResource));
                if (file.Exists)
                    continue;

                using (
                    var stream =
                        Assembly.GetExecutingAssembly()
                            .GetManifestResourceStream(String.Format("{0}.{1}", ResourcePrefix, libResource)))
                using (var fileStream = file.Create())
                {
                    stream.CopyTo(fileStream);
                }
            }

            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
            LessJsSingleton = new LessJs().Engine;
        }

        private LessJs()
        {
            Engine = new V8ScriptEngine();

            var windowJs = new Window(Engine);
            Engine.AddHostType("XMLHttpRequest", typeof (XmlHttpRequest));
            Engine.Script.window = windowJs.Create(Engine);
            Engine.AddHostObject("location", windowJs.Location);
            Engine.AddHostObject("document", windowJs.Document);
            Engine.Execute(GetLessJs());
        }

        public static ScriptEngine LessJsEngine
        {
            get { return LessJsSingleton; }
        }

        public ScriptEngine Engine { get; private set; }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Contains(ClearScriptLibPrefix))
            {
                var path = Environment.Is64BitProcess
                    ? Path.Combine(HttpRuntime.CodegenDir, LibResources[1])
                    : Path.Combine(HttpRuntime.CodegenDir, LibResources[0]);
                if (!File.Exists(path))
                    throw new FileNotFoundException("Cannot find ClearScriptV8 proxy assembly.", path);
                return Assembly.LoadFile(path);
            }
            return null;
        }

        internal static object CreateVariableArgumentJsFunction(Func<object, object> callback, ScriptEngine engine)
        {
            dynamic wrapFunctionCreator = engine.Evaluate("(function(x){return function(){return x(arguments)}})");
            return wrapFunctionCreator(callback);
        }

        private static string GetLessJs()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(LessJsResource))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}