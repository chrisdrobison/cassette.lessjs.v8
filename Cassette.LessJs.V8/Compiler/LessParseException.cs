﻿using System;

namespace Cassette.LessJs.V8.Compiler
{
    public class LessParseException : Exception
    {
        private readonly string _errorType;
        private readonly string _fileName;
        private readonly int _lineNumber;
        private readonly string _message;

        public LessParseException(string message, string fileName, int lineNumber, string errorType)
        {
            _message = message;
            _fileName = fileName;
            _lineNumber = lineNumber;
            _errorType = errorType;
        }

        public override string Message
        {
            get
            {
                return String.Format("{0} error: {1} in {2}, line {3}", _errorType, _message, _fileName,
                    _lineNumber);
            }
        }

        public string FileName { get; set; }

        public int LineNumber { get; set; }
    }
}