﻿using Microsoft.ClearScript;

namespace Cassette.LessJs.V8.Compiler.Stubs
{
    public class Document
    {
        private readonly ScriptEngine _engine;

        public Document(ScriptEngine engine)
        {
            _engine = engine;
        }

        [ScriptMember("getElementsByTagName")]
        public object GetElementsByTagName(string tagName)
        {
            return _engine.Evaluate("[]");
        }
    }
}