﻿using Microsoft.ClearScript;

namespace Cassette.LessJs.V8.Compiler.Stubs
{
    public class Location
    {
        public Location()
        {
            HRef = string.Empty;
            Port = 0;
            Protocol = "file:";
            HostName = "localhost";
        }

        [ScriptMember("hostname")]
        public string HostName { get; set; }

        [ScriptMember("protocol")]
        public string Protocol { get; set; }

        [ScriptMember("port")]
        public int Port { get; set; }

        [ScriptMember("href")]
        public string HRef { get; set; }
    }
}