﻿using System;
using System.IO;
using Microsoft.ClearScript;

namespace Cassette.LessJs.V8.Compiler.Stubs
{
    public class XmlHttpRequest
    {
        [ScriptMember("status")]
        public int Status { get; set; }

        [ScriptMember("responseText")]
        public string ResponseText { get; set; }

        [ScriptMember(Name = "send")]
        public void Send(object ignore)
        {
        }

        [ScriptMember(Name = "setRequestHeader")]
        public void SetRequestHeader(string headerName, string headerValue)
        {
        }

        [ScriptMember(Name = "open")]
        public string Open(string method, string url, bool async)
        {
            var fileName = new Uri(url);

            Status = 200;
            ResponseText = File.ReadAllText(fileName.LocalPath);
            return null;
        }
    }
}