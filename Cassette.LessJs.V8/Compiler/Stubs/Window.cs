﻿using System.Dynamic;
using Microsoft.ClearScript;

namespace Cassette.LessJs.V8.Compiler.Stubs
{
    public class Window : DynamicObject
    {
        public Window(ScriptEngine engine)
        {
            Location = new Location();
            Document = new Document(engine);
        }

        [ScriptMember("document")]
        public Document Document { get; set; }

        [ScriptMember("location")]
        public Location Location { get; set; }

        public object Create(ScriptEngine engine)
        {
            dynamic obj = engine.Evaluate("new Object()");
            var windowJs = new Window(engine);
            obj.document = windowJs.Document;
            obj.location = windowJs.Location;

            obj.XMLHttpRequest = engine.Script.XMLHttpRequest;

            return obj;
        }
    }
}