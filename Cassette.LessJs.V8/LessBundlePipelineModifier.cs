﻿using Cassette.BundleProcessing;
using Cassette.Stylesheets;

namespace Cassette.LessJs.V8
{
    public class LessBundlePipelineModifier : IBundlePipelineModifier<StylesheetBundle>
    {
        public IBundlePipeline<StylesheetBundle> Modify(IBundlePipeline<StylesheetBundle> pipeline)
        {
            var index = pipeline.IndexOf<ParseCssReferences>();
            pipeline.Insert<ParseLessReferences>(index + 1);
            pipeline.Insert<CompileLess>(index + 2);

            return pipeline;
        }
    }
}