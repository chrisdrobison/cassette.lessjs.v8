﻿using Cassette.Stylesheets;

namespace Cassette.LessJs.V8
{
    public class LessFileSearchModifier : IFileSearchModifier<StylesheetBundle>
    {
        public void Modify(FileSearch fileSearch)
        {
            fileSearch.Pattern += ";*.less";
        }
    }
}