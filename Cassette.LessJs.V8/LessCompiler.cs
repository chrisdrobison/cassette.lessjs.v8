﻿using System.Linq;
using System.Web.Hosting;
using Cassette.LessJs.V8.Compiler;

namespace Cassette.LessJs.V8
{
    public class LessCompiler : ILessCompiler
    {
        public CompileResult Compile(string source, CompileContext context)
        {
            var file = HostingEnvironment.MapPath(context.SourceFilePath);
            var parser = new Parser();
            var tree = parser.ParseFileAsync(file).Result;
            var dependencies =
                tree.Dependencies
                    .Select(dep => dep.Replace(HostingEnvironment.ApplicationPhysicalPath, "~/").Replace("\\", "/"));
            return new CompileResult(tree.ToCss(), dependencies);
        }
    }
}